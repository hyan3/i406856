/*
 * Copyright 2022-2023 Penguin Computing, Inc.
 * 45800 Northport Loop West
 * Fremont, CA 94538
 *
 * This material is provided subject to the end user license agreement
 * included with the software.
 */

/* TO-DO
 * Suppress (or reduce frequency) "not a number" and "not >0" printf.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <values.h>
#include <ctype.h>
#include <libgen.h>
#include <errno.h>
#include <string.h>
#include <curl/curl.h>
#include <sys/stat.h>
#include <sys/time.h>

/* #define DEBUG_TIMING 0 */

#define MAX_BUF_SIZE         8192     /* larger is likely bogus */
#define DEFAULT_READ_SIZE    1024     /* for "read" type buffers */
#define DEFAULT_INITIAL_SIZE   64     /* for every other buffer type */
enum buf_add_mode { INIT, APPEND };
enum buf_type {
    READ             = 0,
    COMMAND          = 1,
    CURL_WRITEBACK   = 2,
    CURL_ERROR       = 3,
    WRITEBACK_PARENT = 4,
    HEADNODE_URL     = 5,
    JSON             = 6,
    LOADAVG          = 7,
    UPTIME           = 8,
    NUM_BUF_TYPES    = 9
};
struct buf_clump {
    char *name;
    char *buf;
    size_t size;
};
struct buf_clump buffer[NUM_BUF_TYPES];

enum cached_attrib_list {
    STATUS_SECS        = 0,
    STATUS_CPUSET      = 1,
    STATUS_DEBUG       = 2,
    FORCE_BUSY         = 3,
    PARENT             = 4,
    NUM_CACHED_ATTRIBS = 5
};
enum attrib_type {
    STRING    = 0,
    INT       = 1,
    BOOL      = 2
};
enum attrib_state {
    DEFINED   = 0,
    UNDEFINED = 1
};
enum bool_val_type {
    BOOL_TRUE    = 0,
    BOOL_FALSE   = 1,
    BOOL_INVALID = 2
};
struct cached_attrib {
    char *name;
    enum attrib_state state;
    int found;          /* 1=found on scan pass, 0=otherwise */
    char *str_val;	/* NULL if not found in curl reply or attributes.ini */
    enum attrib_type atype;
    int num_val;       /* attrib val if specified, otherwise default val */
    int num_val_min;
    int num_default;
};
struct cached_attrib cached_attribs[NUM_CACHED_ATTRIBS];

#if 0 /* NOT NEEDED */
enum attrib_validity {
    VALID = 0,
    INVALID = 1,
    TOO_SMALL = 2,
    IS_DEFINED = 3,
    NOT_DEFINED = 4,
    NOT_FOUND = 5,
    NEVER_SET = 6
};
#endif

#define EXIT_RESET_CPUSET 250
char *_status_cpuset = NULL;
char *_status_cpuset_last = NULL;
#if 0  /* NOT NEEDED */
enum attrib_validity writeback_cpuset_defined = NOT_DEFINED;
enum attrib_validity prev_cpuset_defined = NEVER_SET;

enum attrib_validity writeback_parent_defined = NOT_DEFINED;
#endif

#define DEFAULT_STATUS_SECS 10   /* in cw_backend/providers/node.py */
#define MIN_STATUS_SECS      1
#if 0  /* NOT NEEDED */
int _status_secs = DEFAULT_STATUS_SECS;
enum attrib_validity _status_secs_defined = NOT_DEFINED;
#endif

#define DEFAULT_STATUS_DEBUG 0
#define MIN_STATUS_DEBUG     0
int _status_debug = DEFAULT_STATUS_DEBUG;
#if 0  /* NOT NEEDED */
enum attrib_validity _status_debug_defined = NOT_DEFINED;
#endif

/*
 * Currently just use the full pathnames of the various other files and commands
 *  because when we are executed by systemd, our cwd is "/" and argv[0] is the
 *  full path of this program, vs if we are executed manually from some other
 *  location, when we would need to use a combination of dirname() and strcat()
 *  to assemble the directory in which this command actually resides.
 */
#define UPDATE_NODE_STATUS "/opt/scyld/clusterware-node/bin/update-node-status"
#define UPDATE_KEYS        "/opt/scyld/clusterware-node/bin/run-script update_keys"
#define BUSY_FLAG          "/opt/scyld/clusterware-node/etc/busy.flag"
#define ATTRIBUTES_INI     "/opt/scyld/clusterware-node/etc/attributes.ini"
#define HEAD_NODES         "/opt/scyld/clusterware-node/etc/heads.cache"

#define WRITEBACK_FILE     "/opt/scyld/clusterware-node/etc/curl_writeback_file"
                        /* eventually change to .../etc/status.d/curl... */
FILE *curl_writeback_fp = NULL;

#define CMD_DIFF_ATTRIBS      \
          "/usr/bin/diff -q -I ^'parent_now ' " WRITEBACK_FILE " " ATTRIBUTES_INI " >&/dev/null"
#define CMD_OVERWRITE_ATTRIBS "/usr/bin/cp -f " WRITEBACK_FILE " " ATTRIBUTES_INI " >&/dev/null"
#define CMD_BASH_UPDATER      UPDATE_NODE_STATUS " --upload --quiet"
#define CMD_BASH_UPDATER_HW   UPDATE_NODE_STATUS " --hardware --upload --quiet"

struct tosend
{
    size_t pos;
    size_t len;
    char *json;
};

struct curl_response {
    char *memory;
    size_t size;
};

#if defined(DEBUG_TIMING)
long long
tod_in_usecs(void)
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return (tv.tv_sec * 1000000) + tv.tv_usec;
}

static inline long
usec_to_msec(long long usecs)
{
    return (usecs + 499) / 1000;
}
#endif  /* DEBUG_TIMING */

void
init_cached_attribs(void)
{
    int i;
    for (i = 0; i < NUM_CACHED_ATTRIBS; i++) {
        cached_attribs[i].str_val = NULL;
        cached_attribs[i].state   = UNDEFINED;
        cached_attribs[i].found   = 0;
        cached_attribs[i].num_val = 0;

        switch (i)
        {
          case FORCE_BUSY:
            cached_attribs[i].name    = "_busy";
            cached_attribs[i].atype   = BOOL;
            cached_attribs[i].num_default = 0;
            cached_attribs[i].num_val     = 0;
            break;

          case PARENT:
            cached_attribs[i].name    = "parent";
            cached_attribs[i].atype   = STRING;
            break;

          case STATUS_CPUSET:
            cached_attribs[i].name    = "_status_cpuset";
            cached_attribs[i].atype   = STRING;
            break;

          case STATUS_DEBUG:
            cached_attribs[i].name    = "_status_debug";
            cached_attribs[i].atype   = INT;
            cached_attribs[i].num_default = 0;
            cached_attribs[i].num_val     = 0;
            cached_attribs[i].num_val_min = 0;
            break;

          case STATUS_SECS:
            cached_attribs[i].name    = "_status_secs";
            cached_attribs[i].atype   = INT;
            cached_attribs[i].num_default = DEFAULT_STATUS_SECS;
            cached_attribs[i].num_val     = DEFAULT_STATUS_SECS;
            cached_attribs[i].num_val_min = 1;
            break;

          default:
            cached_attribs[i].name    = "???";
            printf("%s: unknown cached attrib type %d\n", __FUNCTION__, i);
            cached_attribs[i].atype   = STRING;
            break;
        }
    }
}

void
init_buffers(void)
{
    int buft;
    for (buft = 0; buft < NUM_BUF_TYPES; buft++) {
        switch (buft)
        {
          case READ:
            buffer[buft].name = "read_buf";
            buffer[buft].buf = calloc(1,DEFAULT_READ_SIZE);
            buffer[buft].size = DEFAULT_READ_SIZE;
            break;

          case COMMAND:
            buffer[buft].name = "command_buf";
            buffer[buft].buf  = calloc(1,DEFAULT_INITIAL_SIZE);
            buffer[buft].size = DEFAULT_INITIAL_SIZE;
            break;

          case CURL_WRITEBACK:
            buffer[buft].name = "curl_writeback_buf";
            buffer[buft].buf  = calloc(1,DEFAULT_READ_SIZE);
            buffer[buft].size = DEFAULT_READ_SIZE;
            break;

          case CURL_ERROR:
            buffer[buft].name = "curl_error_buf";
            buffer[buft].buf  = calloc(1,CURL_ERROR_SIZE+1);
            buffer[buft].size = CURL_ERROR_SIZE;
            break;

          case WRITEBACK_PARENT:
            buffer[buft].name = "writeback_parent_buf";
            buffer[buft].buf  = calloc(1,DEFAULT_INITIAL_SIZE);
            buffer[buft].size = DEFAULT_INITIAL_SIZE;
            break;

          case HEADNODE_URL:
            buffer[buft].name = "headnode_url_buf";
            buffer[buft].buf  = calloc(1,DEFAULT_INITIAL_SIZE);
            buffer[buft].size = DEFAULT_INITIAL_SIZE;
            break;

          case JSON:
            buffer[buft].name = "json_buf";
            buffer[buft].buf  = calloc(1,DEFAULT_INITIAL_SIZE);
            buffer[buft].size = DEFAULT_INITIAL_SIZE;
            break;

          case LOADAVG:
            buffer[buft].name = "loadavg_buf";
            buffer[buft].buf  = calloc(1,DEFAULT_INITIAL_SIZE);
            buffer[buft].size = DEFAULT_INITIAL_SIZE;
            break;

          case UPTIME:
            buffer[buft].name = "uptime_buf";
            buffer[buft].buf  = calloc(1,DEFAULT_INITIAL_SIZE);
            buffer[buft].size = DEFAULT_INITIAL_SIZE;
            break;

          default:
            buffer[buft].name = "???";
            printf("%s: unknown buffer type %d\n", __FUNCTION__, buft);
            buffer[buft].buf = NULL;
            buffer[buft].size = 0;
            break;
        }

        if (buffer[buft].buf == NULL) {
            printf("%s cannot calloc %d bytes for %s!\n",
                   __FUNCTION__, DEFAULT_INITIAL_SIZE, buffer[buft].name);
            exit(1);
        }

        buffer[buft].buf[0] = '\0';   /* stuff string terminator */
    }
}

enum bool_val_type
parse_bool(char *bool_str)
{
    int i;
    for (i = 0; i < strlen(bool_str); i++) { /* nosemgrep flawfinder.strlen-1.wcslen-1._tcslen-1._mbslen-1 # caller guarantees arg is string */ /* Flawfinder: ignore as this has been checked */
        *(bool_str+i) = tolower(*(bool_str+i));
    }
    if (strcmp(bool_str, "t") == 0 || strcmp(bool_str, "true") == 0||
        strcmp(bool_str, "y") == 0 || strcmp(bool_str, "yes")  == 0 ||
        strcmp(bool_str, "1") == 0 || strcmp(bool_str, "on")   == 0) {
        return BOOL_TRUE;
    } else
    if (strcmp(bool_str, "f") == 0 || strcmp(bool_str, "false") == 0 ||
        strcmp(bool_str, "n") == 0 || strcmp(bool_str, "no")    == 0  ||
        strcmp(bool_str, "0") == 0 || strcmp(bool_str, "off")   == 0) {
        return BOOL_FALSE;
    } else {
        return BOOL_INVALID;
    }
}

char *
buf_add_str(enum buf_type buftype, enum buf_add_mode add_mode, char *adding_str)
{
    char *bigger_buf;
    size_t curr_str_len, need_buf_size;
    size_t adding_str_len = strlen(adding_str); /* nosemgrep security/vulnerabilities/75220 # caller guarantees valid string */ /* FlawFinder: ignore as this has been checked */

    if (buffer[buftype].size == 0) {
        /* This shouldn't happen, but deal with it. */
        buffer[buftype].buf = calloc(1, DEFAULT_INITIAL_SIZE);
        if (buffer[buftype].buf == NULL) {
            printf("%s cannot calloc %d bytes for %s!\n", __FUNCTION__,
                   DEFAULT_INITIAL_SIZE, buffer[buftype].name);
            return NULL;
        }
    }

    if (add_mode == INIT) {
        /* stuff string terminator zeroes in allocated buffer */
        memset(buffer[buftype].buf, 0, buffer[buftype].size);
    }
    curr_str_len = strlen(buffer[buftype].buf); /* nosemgrep flawfinder.strlen-1.wcslen-1._tcslen-1._mbslen-1 # buf is guaranteed to contain null-terminated string */ /* FlawFinder: ignore as this has been checked */
    need_buf_size = curr_str_len + adding_str_len + 2;  /* max needed +1 */
    if (need_buf_size >= buffer[buftype].size) {
        /* Buf isn't big enough, so overexpand it. */
        size_t add_size = DEFAULT_INITIAL_SIZE;
        need_buf_size += add_size;
        if (need_buf_size <= MAX_BUF_SIZE) {
            bigger_buf = realloc(buffer[buftype].buf, need_buf_size);
        } else {
            bigger_buf = NULL;
        }
        if (bigger_buf == NULL) {
            printf("%s cannot realloc %ld bytes for %s!\n",
                   __FUNCTION__, need_buf_size,  buffer[buftype].name);
            return NULL;
        } else {
            /* stuff string terminator zeroes in newly allocated bytes */
            memset(bigger_buf + buffer[buftype].size, 0, add_size);
            buffer[buftype].buf  = bigger_buf;
            buffer[buftype].size = need_buf_size;
        }
    }
    /* If we get to here, then the above "max size needed" and if-then
     *  guarantees the dest buffer is large enough.
     */
    if (curr_str_len == 0) {
        strncpy(buffer[buftype].buf, adding_str, adding_str_len+1);  /* nosemgrep flawfinder.strncpy-1 # above "max needed +1" and if-then guarantee dest buf big enough */ /* FlawFinder: ignore as this has been checked */
    } else {
        strncat(buffer[buftype].buf, adding_str, adding_str_len); /* nosemgrep flawfinder.strncpy-1 # above "max needed +1" and if-then guarantee dest buf big enough */ /* FlawFinder: ignore as this has been checked */
    }
    return buffer[buftype].buf;
}

/*
 * General routine to return the first line into the supplied buffer and
 *  return a pointer to the buffer, or return NULL if the open+read fails.
 */
char *
read_file_first_line(char *path)
{
    char *retstr = NULL;
    FILE *fp = fopen(path, "r"); /* nosemgrep flawfinder.fopen-1.open-1 # only opens files in /proc or the heads.cache where the directory is only root writable */ /* FlawFinder: ignore as this has been checked */
    if (fp) {
        while (1) {
            if (buffer[READ].buf) {  /* something should always be allocated */
                memset(buffer[READ].buf, 0, buffer[READ].size);
            }
            retstr = fgets(buffer[READ].buf, buffer[READ].size-1, fp);
            if (retstr) {
                size_t len = strlen(retstr);  /* nosemgrep flawfinder.strlen-1.wcslen-1._tcslen-1._mbslen-1 # fgets guarantees valid string */ /* FlawFinder: ignore as this has been checked */
                if (len < buffer[READ].size-2) {
                    /* We now have a full line that ends with newline. */
                    retstr[len-1] = '\0'; /* replace newline with null */
                    break;
                } else {
                    /* Read buf not big enough.  Make it larger? */
                    size_t new_len = buffer[READ].size + DEFAULT_READ_SIZE;
                    if (new_len > MAX_BUF_SIZE) {
                        retstr = NULL;   /* bogus read */
                        break;
                    } else {
                        buffer[READ].size = new_len;
                        rewind(fp);
                        /* and loop back and re-read the file */
                    }
                }
            } else {
                break;   /* read to end-of-file without finding a line! */
            }
        }
    }
    /* If fall through to here, then we cannot complete the open & read. */
    if (fp) {
        fclose(fp);
    }
    return retstr;
}

char *
get_uptime(void)
{
    char *readbuf = read_file_first_line("/proc/uptime");
    if (readbuf) {
        char *blank;
        /* Stuff end-of-string into the first blank    */
        /*  and return the first number in the buffer. */
        if ((blank = strchr(readbuf, ' '))) {
            *blank = '\0';
            return buf_add_str(UPTIME, INIT, readbuf);
        }
    }
    return "???";
}

char *
get_loadavg(void)
{
    char *readbuf = read_file_first_line("/proc/loadavg");
    if (readbuf) {
        return buf_add_str(LOADAVG, INIT, readbuf);
    } else {
        return "????";
    }
}

/*
 * Return the IP address of the first head node in ${HEAD_NODES}.
 */
char *
get_headnode_url(void)
{
#   define BASE_URL_STR "base_url="
#   define DB_ENDPOINT  "/node/putstatus?fmt=ini"

    char *buf = read_file_first_line(HEAD_NODES);
    if (buf) {
        char *url;
        if ((url = strstr(buf, BASE_URL_STR))) {
            buf_add_str(HEADNODE_URL, INIT, url+strlen(BASE_URL_STR)); /* nosemgrep flawfinder.strlen-1.wcslen-1._tcslen-1._mbslen-1 # buf is guaranteed to contain null-terminated string */ /* FlawFinder: ignore as this has been checked */
            if (buf_add_str(HEADNODE_URL, APPEND, DB_ENDPOINT)) {
                return buffer[HEADNODE_URL].buf;
            } else {
                printf("%s cannot form endpoint string\n", __FUNCTION__);
            }
        } else {
            printf("buf %s has no %s\n", buf, BASE_URL_STR);
            return "No-base_url-found";
        }
    }
    return "No-base_url-found";
}

int
attrib_int_value(enum cached_attrib_list attrib)
{
    if (cached_attribs[attrib].atype == BOOL ||
        cached_attribs[attrib].atype == INT)
    {
        return (cached_attribs[attrib].state == DEFINED)
                  ? cached_attribs[attrib].num_val
                  : cached_attribs[attrib].num_default;
    } else {
        /* string */
        return -9999;
    }
}

char *
attrib_str_value(enum cached_attrib_list attrib)
{
    if (cached_attribs[attrib].atype == STRING) {
        return (cached_attribs[attrib].state == DEFINED)
                  ? cached_attribs[attrib].str_val
                  : NULL;
    } else {
        /* Not a string. */
        return NULL;
    }
}

void
resolve_cached_attribs(void)
{
    int i;
    for (i = 0; i < NUM_CACHED_ATTRIBS; i++) {
        if (cached_attribs[i].found) {
            if (cached_attribs[i].state == UNDEFINED) {
                cached_attribs[i].state = DEFINED;
#if 0
                printf("DBG %s now defined and is %s\n",
                       cached_attribs[i].name, cached_attribs[i].str_val);
#endif
            }
        } else {
            /* not found */
            if (cached_attribs[i].state == DEFINED) {
                if (cached_attribs[i].atype == INT) {
                    if (cached_attribs[i].num_val != cached_attribs[i].num_default) {
                        cached_attribs[i].num_val = cached_attribs[i].num_default;
                        if (attrib_int_value(STATUS_DEBUG)) {
                            printf("%s now undefined and reverts to %d\n",
                                   cached_attribs[i].name, cached_attribs[i].num_val);
                        }
                    }
                } else {
                    cached_attribs[i].state = UNDEFINED;
#if 0
                    printf("DBG %s now undefined\n", cached_attribs[i].name);
#endif
                }
            }
        }
        switch (i)
        {
          case STATUS_CPUSET:
            if (cached_attribs[i].state == DEFINED) {
                /* TODO validate cpuset string */
            }
            break;
        }
    }
}

/*
 * Search buffer for attributes cached locally.
 */
void
find_cached_attribs_in_buffer(char *writeback_buf, size_t buflen)
{
    int i;

    for (i = 0; i < NUM_CACHED_ATTRIBS; i++) {
        char *attrib_start;
        cached_attribs[i].found = 0;   /* unless we find it */

        /* Is the attribute in this buffer? */
        if ((attrib_start = strstr(writeback_buf, cached_attribs[i].name))) {
            /* Cached attrib may match. Followed by a "="? */
            char *eqp;
            if ((eqp = strchr(attrib_start, '='))) {
                /* Isolate the attrib name in buf to determine if true match */
                char *finder;
                char *extract_name = calloc(1, eqp-attrib_start+1);
                memcpy(extract_name, attrib_start, eqp-attrib_start); /* nosemgrep flawfinder.memcpy-1.CopyMemory-1.bcopy-1 # 76028 destination guaranteed to be large enough */ /* FlawFinder: ignore as this has been checked */
                finder = extract_name+1;
                while (1) {
                    if (*finder == ' ') {
                        *finder = '\0';  /* stuff end-of-string */
                    }
                    if (*finder == '\0') break;
                    finder++;
                }
                if (strcmp(cached_attribs[i].name, extract_name) == 0) {
                    /* We have a confirmed match to the cached name */
                    char *valp, *valendp, *extract_val;
                    char *bufend = writeback_buf + buflen;
                    /* Find the value */
                    valp = eqp+1;
                    while (*valp == ' ') valp++;
                    /* valp points at start of value.  Search for end. */
                    valendp = valp+1;
                    while (valendp < bufend) {
                        if (*valendp == '\n' || *valendp == ' ') break;
                        valendp++;
                    }
                    extract_val = calloc(1, valendp-valp+1);
                    memcpy(extract_val, valp, valendp-valp); /* nosemgrep flawfinder.memcpy-1.CopyMemory-1.bcopy-1 # 76028 destination guaranteed to be large enough */ /* FlawFinder: ignore as this has been checked */
                    if (cached_attribs[i].str_val) {
                        if (strcmp(cached_attribs[i].str_val, extract_val)) {
                            /* Replace previous value with diff value. */
                            free(cached_attribs[i].str_val);
                            cached_attribs[i].str_val = extract_val;
                        }
                    } else {
                        cached_attribs[i].str_val = extract_val;
                    }
                    if (cached_attribs[i].atype == STRING) {
                        cached_attribs[i].found  = 1;
                        if (attrib_int_value(STATUS_DEBUG)) {
                            printf("%s has new value %s\n",
                                   cached_attribs[i].name,
                                   cached_attribs[i].str_val);
                        }
                    } else
                    if (cached_attribs[i].atype == INT) {
                        char *endptr;
                        long val_long = strtol(valp, &endptr, 10);
                        if (*endptr == '\0' || *endptr == '\r' || *endptr == '\n') {
                            if (val_long < cached_attribs[i].num_val_min) {
                                printf("%s val=%d less than min=%d, use default\n",
                                       cached_attribs[i].name, (int)val_long,
                                       cached_attribs[i].num_default);
                                cached_attribs[i].found = 0;
                            } else {
                                if (cached_attribs[i].num_val != (int)val_long) {
                                    cached_attribs[i].num_val = (int)val_long;
                                    if (attrib_int_value(STATUS_DEBUG)) {
                                        printf("%s has new value %d\n",
                                               cached_attribs[i].name,
                                               cached_attribs[i].num_val);
                                    }
                                }
                            }
                            cached_attribs[i].found = 1;
                        } else {
                            cached_attribs[i].found = 0;
                        }
                    } else
                    if (cached_attribs[i].atype == BOOL) {
                        enum bool_val_type bool_val = parse_bool(cached_attribs[i].str_val); /* nosemgrep Flawfinder-strlen # 76027 caller guarantees buffer is valid string */
                        if (bool_val == BOOL_TRUE) {
                            cached_attribs[i].num_val = 1;
                            cached_attribs[i].found   = 1;
                        } else
                        if (bool_val == BOOL_FALSE) {
                            cached_attribs[i].num_val = 0;
                            cached_attribs[i].found   = 1;
                        }
#if 0
                        if (cached_attribs[i].found &&
                            cached_attribs[i].state == UNDEFINED)
                        {
                            printf("DBG %s %s is boolean '%s' %d\n", __FUNCTION__,
                                   cached_attribs[i].name,
                                   cached_attribs[i].str_val,
                                   cached_attribs[i].num_val);
                        }
#endif
                    }
                }
                free(extract_name);
            }
        }
   }
   resolve_cached_attribs();
}

void
find_cached_attribs_in_file(void)
{
    struct stat stat_buf;
    FILE *fp;

    if (stat((const char *)ATTRIBUTES_INI, &stat_buf) == 0 &&
        (fp = fopen(ATTRIBUTES_INI, "r"))) /* nosemgrep flawfinder.fopen-1.open-1 # only opens file in directory which is only root writable */ /* FlawFinder: ignore as this has been checked */
    {
        /*
         * We want to read the ATTRIBUTES_INI file into the writeback buffer
         *  and search for cached attributes in that buffer.  Is it big enough?
         */
        size_t need_buf_size = stat_buf.st_size + 8;
        if (need_buf_size > buffer[CURL_WRITEBACK].size) {
            char *new_buf = realloc(buffer[CURL_WRITEBACK].buf, need_buf_size);
            if (new_buf == NULL) {
                printf("%s cannot realloc %ld bytes for %s!\n", __FUNCTION__,
                       need_buf_size, buffer[CURL_WRITEBACK].name);
            } else {
                buffer[CURL_WRITEBACK].size = need_buf_size;
            }
        }
        /* Verify that we have a large enough read buffer. */
        if (need_buf_size <= buffer[CURL_WRITEBACK].size) {
            size_t read_size;
            read_size = fread(buffer[CURL_WRITEBACK].buf, 1, need_buf_size-1, fp);
            if (read_size > 0 && read_size < need_buf_size-1) {
                *(buffer[CURL_WRITEBACK].buf + read_size) = '\0';
                find_cached_attribs_in_buffer(buffer[CURL_WRITEBACK].buf,
                                              buffer[CURL_WRITEBACK].size);
            }
        }

        fclose(fp);
    }
}

/*
 * Search attributes.ini for attributes cached locally.
 */
void
search_attrib_file_for_cached_attribs(void)
{
    /* int attrib_num; */
    FILE *fp = fopen(ATTRIBUTES_INI, "r"); /* nosemgrep flawfinder.fopen-1.open-1 # only opens file in directory which is only root writable */ /* FlawFinder: ignore as this has been checked */

    if (fp) {
        while (1) {
            char *fgets_ret;
            fgets_ret = fgets(buffer[READ].buf, buffer[READ].size-1, fp);
            if (fgets_ret) {
                size_t len = strlen(fgets_ret);   /* nosemdep flawfinder.strlen-1.wcslen-1._tcslen-1._mbslen-1 # fgets guarantees valid */ /* Flawfinder: ignore as this has been checked */
                if (len < buffer[READ].size-2) {
                    /* Have a full line that ends with newline */
                    fgets_ret[len-1] = '\0';
                } else {
                    /* Buf not big enough.  Make it larger? */
                    size_t new_len = buffer[READ].size + DEFAULT_READ_SIZE;
                    if (new_len > MAX_BUF_SIZE) {
                        fgets_ret = NULL;   /* bogus read */
                        break;
                    } else {
                        buffer[READ].size = new_len;
                        rewind(fp);
                        /* and loop back and re-read the file */
                    }
                }

                /* TODO */

                /* Otherwise NOT_FOUND, so keep reading the file */
            } else {
                break;  /* at end-of-file */
            }
        }
    }
    if (fp) {
        fclose(fp);
    }
}

/*
 * Return 1 if _busy attribute is set or node-busy flag file exists,
 *        0 otherwise.
 */
int
check_node_busy(void)
{
    if (cached_attribs[FORCE_BUSY].state == DEFINED &&
        cached_attribs[FORCE_BUSY].num_val == 1)
    {
        return 1;
    } else {
        struct stat stat_buf;
        return (stat((const char *)BUSY_FLAG, &stat_buf) == 0) ? 1 : 0;
    }
}

size_t
read_callback(char *buffer, size_t size, size_t nitems, void *userdata)
{
    struct tosend *content = (struct tosend*)userdata;
    char *start = content->json + content->pos;
    size_t amt = size * nitems;
    size_t max = strlen(start); /* nosemgrep flawfinder.strlen-1.wcslen-1._tcslen-1._mbslen-1 # 'start' is orig json which is guaranteed by outer loop to be string */ /* FlawFinder: ignore as this has been checked */
    if (amt > max)
        amt = max;

    memcpy(buffer, content->json, amt); /* nosemgrep flawfinder.memcpy-1.CopyMemory-1.bcopy-1 # 'amt' is guaranteed smaller that caller's size*nitems max */ /* FlawFinder: ignore as this has been checked */
    content->pos += amt;
    return amt;
}

size_t
write_callback(char *ptr, size_t size, size_t nmemb, void *userdata)
{
    size_t curr_buf_inuse;
    size_t new_writeback_len, new_writeback_bufsize;
    char *new_buf = NULL;

    if (curl_writeback_fp) {
        fwrite(ptr, size, nmemb, curl_writeback_fp);
    }

    curr_buf_inuse = strlen(buffer[CURL_WRITEBACK].buf); /* nosemgrep flawfinder.strlen-1.wcslen-1._tcslen-1._mbslen-1 # this routine guaranees buf always valid string */ /* Flawfinder: ignore as this has been checked */
    new_writeback_len = size * nmemb;
    new_writeback_bufsize = curr_buf_inuse + new_writeback_len + 1;
    if (curr_buf_inuse + new_writeback_len + 1 >= buffer[CURL_WRITEBACK].size) {
        /* Overexpand the current buffer. */
        new_writeback_bufsize += DEFAULT_INITIAL_SIZE;
        new_buf = realloc(buffer[CURL_WRITEBACK].buf, new_writeback_bufsize);
        if (new_buf) {
            buffer[CURL_WRITEBACK].buf  = new_buf;
            buffer[CURL_WRITEBACK].size = new_writeback_bufsize;
        } else {
            printf("%s cannot realloc %ld bytes!", __FUNCTION__, new_writeback_bufsize);
            return size * nmemb;
        }
    }

    /* Writeback buf is big enough to hold the new data */
    memcpy((void *)&buffer[CURL_WRITEBACK].buf[curr_buf_inuse], /* nosemgrep flawfinder.memcpy-1.CopyMemory-1.bcopy-1 # allocated sufficient space above */ /* FlawFinder: ignore as this has been checked */
           ptr, new_writeback_len);
    /* Add string terminator for safety. */
    buffer[CURL_WRITEBACK].buf[curr_buf_inuse + new_writeback_len + 1] = '\0';
    return size * nmemb;
}

CURL *curl_init(void)
{
    CURL *curl;

    /* get a curl handle */
    curl = curl_easy_init();
    if(curl) {
        curl_easy_setopt(curl, CURLOPT_USERAGENT, "status-updater/2.0");
        curl_easy_setopt(curl, CURLOPT_UPLOAD, 1);
        curl_easy_setopt(curl, CURLOPT_LOCALPORT, 768);
        curl_easy_setopt(curl, CURLOPT_LOCALPORTRANGE, 255);
        curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);
        curl_easy_setopt(curl, CURLOPT_READFUNCTION, read_callback);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_callback);
        curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, buffer[CURL_ERROR].buf);
        curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1);
        return curl;
    } else {
        return NULL;
    }
}

void
check_head_node_uid(void)
{
    FILE *fp;
    char *parent_attrib = NULL;

    /* Extract the parent's UID from the response writeback. */
    buf_add_str(WRITEBACK_PARENT, INIT, "head_");
    if ((parent_attrib = attrib_str_value(PARENT)) != NULL)
    {
        /* Form the string we will search for in heads.cache */
        buf_add_str(WRITEBACK_PARENT, APPEND, parent_attrib);
        buf_add_str(WRITEBACK_PARENT, APPEND, " ");
    } else {
        /* For some reason the response didn't include "parent" attribute. */
        return;
    }

    fp = fopen(HEAD_NODES, "r"); /* nosemgrep flawfinder.fopen-1.open-1 # only opens file in directory which is only root writable */ /* FlawFinder: ignore as this has been checked */
    if (fp) {
        while (1) {
            char *fgets_ret = fgets(buffer[READ].buf, buffer[READ].size-1, fp);
            if (fgets_ret) {
                size_t len = strlen(fgets_ret);   /* nosemgrep flawfinder.strlen-1.wcslen-1._tcslen-1._mbslen-1 # fgets guarantees valid */ /* FlawFinder: ignore as this has been checked */
                if (len < buffer[READ].size-2) {
                    /* Have a full line that ends with newline. */
                    fgets_ret[len-1] = '\0';   /* replace newline with null */
                } else {
                    /* Buf not big enough.  Make it larger? */
                    size_t new_len = buffer[READ].size + DEFAULT_READ_SIZE;
                    if (new_len > MAX_BUF_SIZE) {
                        fgets_ret = NULL;   /* bogus read */
                        break;
                    } else {
                        buffer[READ].size = new_len;
                        rewind(fp);
                        /* and loop back and re-read the file */
                    }
                }
                if (strstr(buffer[READ].buf, buffer[WRITEBACK_PARENT].buf)) {
                    /* Found a match, so everything is okay. */
                    fclose(fp);
                    return;
                }
            } else {
                break;   /* read to end-of-file */
            }
        }
        fclose(fp);
    }

    /* If we fall through to here, then the parent's UID isn't in heads.cache,
     *  so we need to update the keys.
     *  TODO: Implement this in C code.
     *
     * We're in busy mode.  Make a tactical decision to ignore this error
     *  so as to avoid possible jitter, versus doing:
     *       if (system(UPDATE_KEYS) < 0) {
     *           printf("%s fails to update keys!\n", UPDATE_KEYS);
     *       }
     */
}

void
choose_most_recent_attributes_file(void)
{
    struct stat stat_buf;
    long busy_mode_modify_time, attributes_modify_time;

    if (stat((const char *)WRITEBACK_FILE, &stat_buf) == 0) {
        busy_mode_modify_time = stat_buf.st_mtime;
    } else {
        return;
    }

    if (stat((const char *)ATTRIBUTES_INI, &stat_buf) == 0) {
        attributes_modify_time = stat_buf.st_mtime;
    } else {
        /* Official attributes file doesn't exist, so copy WRITEBACK_FILE
         * to create it.  Use a st_mtime of zero so writeback file always wins.
         */
        attributes_modify_time = 0;
    }

    if (busy_mode_modify_time > attributes_modify_time) {
        /* We're not in busy mode, so it's okay to use system().
         * Only overwrite attributes.ini if there a difference
         *  in the two files other than "parent_now".
         *
         *  TODO: For now, do the 'diff' explicitly ignoring any "parent_now"
         *        line so as to execute in a pre-cw12 environment for any
         *        compute node needing optimal performance.  When cw12 becomes
         *        the norm, then attributes.ini no longer has "parent_now".
         */
        if (system( CMD_DIFF_ATTRIBS )) {  /* 'diff' of only-root-writeable files */ /* nosemgrep flawfinder.system-1 # uses a constant string with a full path */ /* FlawFinder: ignore */
            if (_status_debug) {
                printf("Overwrite %s with differing newer %s\n",
                       basename(ATTRIBUTES_INI), basename(WRITEBACK_FILE));
            }
            system( CMD_OVERWRITE_ATTRIBS );  /* 'cp' to only-root-writeable file */ /* nosemgrep flawfinder.system-1 # uses a constant string with a full path */ /* FlawFinder: ignore */
        }
    }
}

void
handle_curl_error(CURLcode curl_ret)
{
    printf("curl_easy_perform() failed: %s\n%s\n",
           curl_easy_strerror(curl_ret), buffer[CURL_ERROR].buf);

    /* For now, don't do anything beyond making the error visible to systemd.
     * We're in busy mode, and it's too expensive to run shell commands
     *  that have a potential of causing jitter.
     */

#if 0
    switch (curl_ret)
    {
      case CURLE_COULDNT_CONNECT:
        /* need to do something */
        break;

      default:
        printf("todo: Curl error '%d'\n", curl_ret);
        break;
    }
#endif

    /* TODO: switch to another head node
     * TODO: changing the route and DNS should move into cycle_head_cache
     * TODO: only change the DNS if the old head node is the only name server
     */
}

int
main(int argc, char *argv[])
{
    CURLcode curl_ret;
    CURL *curl = NULL;
    struct curl_response memchunk;
    int sendhw, upload_status_ret;

#if defined(DEBUG_TIMING)
    long long busy_time_start, busy_time_this, busy_time_total = 0;
    long long curl_time_start, curl_time_this, curl_time_total = 0;
    long long norm_time_start, norm_time_this, norm_time_total = 0;
    long long busy_time_min = MAXLONG, busy_time_max = 0;
    long long curl_time_min = MAXLONG, curl_time_max = 0;
    long long norm_time_min = MAXLONG, norm_time_max = 0;
    long busy_time_count = 0, norm_time_count = 0;
#   define REPORT_INTERVAL 100
#endif  /* DEBUG_TIMING */

    init_buffers();
    init_cached_attribs();
    find_cached_attribs_in_file();

    /* Initialize _status_cpuset_last and prev_cpuset_defined from the current
     *  attributes file, if it exists, so we can detect any difference when we
     *  do the first curl update and get the latest head node value.
     */
    _status_cpuset_last = attrib_str_value(STATUS_CPUSET);

    /* In windows, this will init the winsock stuff */
    curl_global_init(CURL_GLOBAL_ALL);

    sendhw = 1;      /* always send hardware info at start up */

    while (1) {
        if (check_node_busy()) {
            struct tosend content = { 0, 0, NULL };
                                 /* { pos, len, json } */

#if defined(DEBUG_TIMING)
            busy_time_start = tod_in_usecs();
#endif  /* DEBUG_TIMING */

            buf_add_str(JSON, INIT,   "{\"status\": {");
            buf_add_str(JSON, APPEND, "\"update\":\"merge\"");
            buf_add_str(JSON, APPEND, ",\"state\":\"up\"");
            buf_add_str(JSON, APPEND, ",\"loadavg\":\"");
            buf_add_str(JSON, APPEND, get_loadavg());
            buf_add_str(JSON, APPEND, "\"");
            buf_add_str(JSON, APPEND, ",\"uptime\":\"");
            buf_add_str(JSON, APPEND, get_uptime());
            content.json = buf_add_str(JSON, APPEND, "\"}}");
            content.len  = strlen(content.json); /* nosemgrep flawfinder.strlen-1.wcslen-1._tcslen-1._mbslen-1 # buf_addr_str guarantees valid string */ /* FlawFinder: ignore as this has been checked */
            if (_status_debug > 1) {
                printf("json:'%s'\n", content.json);
            }
            if (!curl) {
                curl = curl_init();
            }
            if (curl) {
                if (curl_writeback_fp) {
                    /* Writeback file exists - just reset to zero length. */
                    fseek(curl_writeback_fp, 0L, SEEK_SET);
                } else {
                    /* Writeback file doesn't exist - create it. */
                    curl_writeback_fp = fopen(WRITEBACK_FILE, "w+"); /* nosemgrep flawfinder.fopen-1.open-1 # only opens file in directory which is only root writable */ /* FlawFinder: ignore as this has been checked */

                }
                if (curl_writeback_fp) {
                    buffer[CURL_WRITEBACK].buf[0] = '\0';  /* reset buf to empty */
                    memchunk.memory = buffer[CURL_WRITEBACK].buf;
                    memchunk.size   = buffer[CURL_WRITEBACK].size;
                    curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&memchunk);
                }
                curl_easy_setopt(curl, CURLOPT_URL, get_headnode_url());
                curl_easy_setopt(curl, CURLOPT_READDATA, (void*)&content);
                curl_easy_setopt(curl, CURLOPT_INFILESIZE, (curl_off_t)content.len);
                buffer[CURL_ERROR].buf[0] = '\0';    /* to be safe */

                /*
                 * EXECUTE CURL SENDING MINIMAL STATUS INFO
                 */
                if (_status_debug > 1) {
                    printf("execute `curl_easy_perform`\n");
                }
#if defined(DEBUG_TIMING)
                curl_time_start = tod_in_usecs();;
#endif  /* DEBUG_TIMING */

                curl_ret = curl_easy_perform(curl);

#if defined(DEBUG_TIMING)
                curl_time_this = tod_in_usecs() - curl_time_start;
                if (curl_time_min > curl_time_this) {
                    curl_time_min = curl_time_this;
                }
                if (curl_time_max < curl_time_this) {
                    curl_time_max = curl_time_this;
                }
                curl_time_total += curl_time_this;
#endif  /* DEBUG_TIMING */

                fclose(curl_writeback_fp);
                curl_writeback_fp = NULL;

                if (curl_ret == CURLE_OK) {
#if 0                /* XXX expensive and not dealing with result - just skip */
                     /* If the parent's UID isn't in the heads.cache file,
                      *  then we need to re-read the keys.
                      */
                     check_head_node_uid();
#endif

                    find_cached_attribs_in_buffer(buffer[CURL_WRITEBACK].buf,
                                                  buffer[CURL_WRITEBACK].size);
                } else {
                    handle_curl_error(curl_ret);
                }
            } else {
               printf("curl not initialized!\n");
            }

#if defined(DEBUG_TIMING)
            busy_time_this = tod_in_usecs() - busy_time_start;
            if (busy_time_min > busy_time_this) {
                busy_time_min = busy_time_this;
            }
            if (busy_time_max < busy_time_this) {
                busy_time_max = busy_time_this;
            }
            busy_time_total += busy_time_this;
            if (++busy_time_count >= REPORT_INTERVAL && _status_debug) {
                printf("busy mode time mean=%ld min=%lld max=%lld msecs\n",
                       usec_to_msec(busy_time_total / busy_time_count),
                       usec_to_msec(busy_time_min), usec_to_msec(busy_time_max));
                printf("     curl time mean=%ld min=%lld max=%lld msecs\n",
                       usec_to_msec(curl_time_total / busy_time_count),
                       usec_to_msec(curl_time_min), usec_to_msec(curl_time_max));
                curl_time_total = 0;
                curl_time_min   = MAXLONG;
                curl_time_max   = 0;
                busy_time_count = 0;
                busy_time_total = 0;
                busy_time_min   = MAXLONG;
                busy_time_max   = 0;
            }
#endif  /* DEBUG_TIMING */
        } else {
            /*
             * EXECUTE THE EXPENSIVE 'update-node-status' BASH SCRIPT
             */

#if defined(DEBUG_TIMING)
            norm_time_start = tod_in_usecs();
#endif  /* DEBUG_TIMING */

            /* If the busy-mode "curl_writeback_file" newer than "attributes.ini",
             *  then copy the former to the latter.
             */
            choose_most_recent_attributes_file();

            /* Execute 'update-node-status' */
            if (_status_debug > 1) {
                printf("execute '%s'\n",
                       (sendhw) ? CMD_BASH_UPDATER_HW : CMD_BASH_UPDATER);
            }
            upload_status_ret = (sendhw) ? system( CMD_BASH_UPDATER_HW ) /* nosemgrep flawfinder.system-1 # uses a constant string with a full path */ /* FlawFinder: ignore */
                                         : system( CMD_BASH_UPDATER ); /* nosemgrep flawfinder.system-1 # uses a constant string with a full path */ /* FlawFinder: ignore */

            if (upload_status_ret != 0) {
                sendhw = 1;
            } else {
                sendhw = 0;
            }

            find_cached_attribs_in_file();

#if defined(DEBUG_TIMING)
            norm_time_this = tod_in_usecs() - norm_time_start;
            if (norm_time_min > norm_time_this) {
                norm_time_min = norm_time_this;
            }
            if (norm_time_max < norm_time_this) {
                norm_time_max = norm_time_this;
            }
            norm_time_total += norm_time_this;
            if (++norm_time_count >= REPORT_INTERVAL && _status_debug) {
                printf("normal mode time mean=%ld min=%lld max=%lld msecs\n",
                       usec_to_msec(norm_time_total / norm_time_count),
                       usec_to_msec(norm_time_min), usec_to_msec(norm_time_max));
                norm_time_count = 0;
                norm_time_total = 0;
                norm_time_min   = MAXLONG;
                norm_time_max   = 0;
            }
#endif  /* DEBUG_TIMING */
        }

        /* Has the desired cpuset changed? */
        _status_cpuset = attrib_str_value(STATUS_CPUSET);
        if ( (_status_cpuset != NULL && _status_cpuset_last == NULL) ||
             (_status_cpuset == NULL && _status_cpuset_last != NULL) ||
             (_status_cpuset && _status_cpuset_last &&
              strcmp(_status_cpuset, _status_cpuset_last) != 0)
           ) {
            /* Mismatch of prev cpuset and newest, so mismatch exit. */
            if (attrib_int_value(STATUS_DEBUG)) {
                printf("_status_cpuset has changed, so restart\n");
            }
            exit(EXIT_RESET_CPUSET);
        }

        if (attrib_int_value(STATUS_DEBUG) > 1) {
            printf("sleep %d seconds\n", attrib_int_value(STATUS_SECS));
        }
        fflush(stdout);

        sleep(attrib_int_value(STATUS_SECS));
    }

    if (curl) {
        curl_easy_cleanup(curl);
    }
    curl_global_cleanup();

    return 0;
}
